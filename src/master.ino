/* Particle libraries */
#include "Particle.h"

#include <Wire.h>

/* Local libraries */
#include "particle-management/management.h"

// Pin, addresses & settings
#define BTN_COUNT_ADR 0
#define LED1 A4
#define LED2 A5
#define LED3 16
#define LED4 17
#define BTN1 D3
#define BTN2 D2
#define BTN3 D5
#define BTN4 D4
#define BTN1_TOPIC "event-1"
#define BTN2_TOPIC "event-2"
#define BTN3_TOPIC "event-3"
#define BTN4_TOPIC "event-4"

SYSTEM_MODE(SEMI_AUTOMATIC);

/* Global variables */
struct btnCounter {
  uint8_t written;
  uint16_t btn1;
  uint16_t btn2;
  uint16_t btn3;
  uint16_t btn4;
};
btnCounter btnCountZero = {0, 0, 0, 0, 0};
btnCounter btnCount = {255, 0, 0, 0, 0};
int btnPress[5] = {0, 0, 0, 0, 0};
int lastRuntime = 0;
Management management;

void setup()
{
  // Turn on status LED during setup
  pinMode(D7, OUTPUT);
  digitalWrite(D7, HIGH);

  // Turn on external LED during setup
  pinMode(LED1, OUTPUT); pinMode(LED2, OUTPUT);
  pinMode(LED3, OUTPUT); pinMode(LED4, OUTPUT);
  digitalWrite(LED1, HIGH); digitalWrite(LED2, HIGH);
  digitalWrite(LED3, HIGH); digitalWrite(LED4, HIGH);

  // Set inputs
  pinMode(BTN1, INPUT_PULLUP);
  pinMode(BTN2, INPUT_PULLUP);
  pinMode(BTN3, INPUT_PULLUP);
  pinMode(BTN4, INPUT_PULLUP);

  // Recover button presses from EEPROM
  EEPROM.get(BTN_COUNT_ADR, btnCount);
  if (btnCount.written != 0) {
    EEPROM.put(BTN_COUNT_ADR, btnCountZero);
    btnCount = btnCountZero;
  }

  management.MQTTCallback = callback;

  if (management.connect(SystemMode.mode()) != 0) System.reset();
  System.on(all_events, handlerEvents);

  management.publishInfo();
  management.subscribeMQTT("control");

  management.publish(String::format("%s-count", BTN1_TOPIC), btnCount.btn1);
  management.publish(String::format("%s-count", BTN2_TOPIC), btnCount.btn2);
  management.publish(String::format("%s-count", BTN3_TOPIC), btnCount.btn3);
  management.publish(String::format("%s-count", BTN4_TOPIC), btnCount.btn4);

  // Turn off status LED
  digitalWrite(D7, LOW);
  digitalWrite(LED1, LOW); digitalWrite(LED2, LOW);
  digitalWrite(LED3, LOW); digitalWrite(LED4, LOW);
}

void loop()
{
  management.loopStart();
  if (!management.isConnected()) System.reset();

  int runtime = management.runtime();
  if (runtime - lastRuntime >= PUB_RUNTIME_INTERVAL) {
    lastRuntime = runtime;
    management.publish("runtime", runtime);
    management.publish(String::format("%s-count", BTN1_TOPIC), btnCount.btn1);
    management.publish(String::format("%s-count", BTN2_TOPIC), btnCount.btn2);
    management.publish(String::format("%s-count", BTN3_TOPIC), btnCount.btn3);
    management.publish(String::format("%s-count", BTN4_TOPIC), btnCount.btn4);
  }

  buttonsPressed(btnPress);

  // Publish raw button presses if PUB_BUTTONS defined
  #ifdef PUB_BUTTONS
  if (btnPress[0]) {
    management.publish(
      "buttons",
      String::format("%d:%d:%d:%d:%d", btnPress[0], btnPress[1], btnPress[2], btnPress[3], btnPress[4])
    );
  }
  #endif

  // Continue loop() if no button was pressed
  if (!btnPress[0]) return;

  if (btnPress[0] == 2 && btnPress[1] && btnPress[3]) {
    // Reset counters if buttons 1 and 3 are pressed together for more than RESET_TIME
    #ifdef RESET_ENABLE
    digitalWrite(LED1, HIGH); digitalWrite(LED2, HIGH);
    digitalWrite(LED3, HIGH); digitalWrite(LED4, HIGH);
    bool isReset = true;
    for (int t = millis(); millis() - t < RESET_TIME;) {
      management.keepAlive();
      if (digitalRead(BTN1) || digitalRead(BTN3)) {
        isReset = false;
        break;
      }
      delay(100);
    }
    if (isReset) {
      // Flash LED once RESET_TIME has elapsed until both buttons are depressed
      while (!digitalRead(BTN1) || !digitalRead(BTN3)) {
        management.keepAlive();
        delay(50);
        digitalWrite(LED1, LOW); digitalWrite(LED2, LOW);
        digitalWrite(LED3, LOW); digitalWrite(LED4, LOW);
        delay(50);
        digitalWrite(LED1, HIGH); digitalWrite(LED2, HIGH);
        digitalWrite(LED3, HIGH); digitalWrite(LED4, HIGH);
      }
      btnCount = btnCountZero;
      management.publish("reset", true);
      management.publish(String::format("%s-count", BTN1_TOPIC), btnCount.btn1);
      management.publish(String::format("%s-count", BTN2_TOPIC), btnCount.btn2);
      management.publish(String::format("%s-count", BTN3_TOPIC), btnCount.btn3);
      management.publish(String::format("%s-count", BTN4_TOPIC), btnCount.btn4);
    }
    #else
    return;
    #endif
  } else if (btnPress[0] > 1) {
    return;
  } else if (btnPress[1]) {
    digitalWrite(LED1, HIGH);
    btnCount.btn1++;
    management.publish(BTN1_TOPIC, 1);
    management.publish(String::format("%s-count", BTN1_TOPIC), btnCount.btn1);
  } else if (btnPress[2]) {
    digitalWrite(LED2, HIGH);
    btnCount.btn2++;
    management.publish(BTN2_TOPIC, 1);
    management.publish(String::format("%s-count", BTN2_TOPIC), btnCount.btn2);
  } else if (btnPress[3]) {
    digitalWrite(LED3, HIGH);
    btnCount.btn3++;
    management.publish(BTN3_TOPIC, 1);
    management.publish(String::format("%s-count", BTN3_TOPIC), btnCount.btn3);
  } else if (btnPress[4]) {
    digitalWrite(LED4, HIGH);
    btnCount.btn4++;
    management.publish(BTN4_TOPIC, 1);
    management.publish(String::format("%s-count", BTN4_TOPIC), btnCount.btn4);
  }

  EEPROM.put(BTN_COUNT_ADR, btnCount);

  do {
    buttonsPressed(btnPress);
    management.keepAlive();
  } while (btnPress[0]);

  digitalWrite(LED1, LOW); digitalWrite(LED2, LOW);
  digitalWrite(LED3, LOW); digitalWrite(LED4, LOW);

  management.loopClose();
}

int buttonsPressed(int b[5])
{
  b[1] = 0;
  b[2] = 0;
  b[3] = 0;
  b[4] = 0;

  for (int t = millis(); millis() - t < 100;) {
    b[1] = b[1] || !digitalRead(BTN1);
    b[2] = b[2] || !digitalRead(BTN2);
    b[3] = b[3] || !digitalRead(BTN3);
    b[4] = b[4] || !digitalRead(BTN4);
  }

  b[0] = b[1] + b[2] + b[3] + b[4];

  return b[0];
}

void callback(char* topic, byte* payload, unsigned int length) {
  char p[length + 1];
  memcpy(p, payload, length);
  p[length] = NULL;
  String p_str = String(p);
  if (p_str == "reset") System.reset();
  else if (p_str == "signal") blinkLED.setActive(!blinkLED.isActive());
  else if (p_str == "runtime") management.publish("runtime", management.runtime());
  else if (p_str == "count") {
    management.publish(String::format("%s-count", BTN1_TOPIC), btnCount.btn1);
    management.publish(String::format("%s-count", BTN2_TOPIC), btnCount.btn2);
    management.publish(String::format("%s-count", BTN3_TOPIC), btnCount.btn3);
    management.publish(String::format("%s-count", BTN4_TOPIC), btnCount.btn4);
  }
  else management.publish("console", "ERROR:" + p_str);
}

// Handle all events
void handlerEvents(system_event_t event, int data)
{
  // Trigger a reset if cloud or WiFi/cellular connections change status
  if (event == cloud_status_disconnected) System.reset();
  else if (event == network_status_disconnected) System.reset();
  else if (event == reset_pending) management.disconnect();
}
